package com.app.androidcomponents.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.app.androidcomponents.model.ArticlesItem
import com.app.androidcomponents.R
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.news_items.view.*

class NewsListAdapter(private val newsList: ArrayList<ArticlesItem>) :
    RecyclerView.Adapter<NewsListAdapter.NewsViewHolder>() {


    fun updateList(list: List<ArticlesItem>) {
        list.let {
            newsList.clear()
            newsList.addAll(it)
            notifyDataSetChanged()
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.news_items, parent, false)
        return NewsViewHolder(view)
    }

    override fun getItemCount() = newsList.size

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        holder.itemView.headlines.text = newsList[position].title
        Glide.with(holder.itemView.imageView.context).load(newsList[position].urlToImage).error(R.mipmap.ic_launcher)
            .into(holder.itemView.imageView)

        holder.itemView.setOnClickListener {
            Toast.makeText(holder.itemView.context, "Clicked $position", Toast.LENGTH_SHORT).show()
            Navigation.findNavController(it)
                .navigate(ListFragmentDirections.actionListFragmentToDetailsFragment())
        }
    }

    class NewsViewHolder(view: View) : RecyclerView.ViewHolder(view)

}