package com.app.androidcomponents.view


import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.androidcomponents.R
import com.app.androidcomponents.model.ArticlesItem
import com.app.androidcomponents.viewModel.ListViewModel
import kotlinx.android.synthetic.main.fragment_list.*
import java.io.File


class ListFragment : Fragment() {

    lateinit var listViewModel: ListViewModel


    var myList = arrayListOf<ArticlesItem>()
    var adapter = NewsListAdapter(myList)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        listViewModel = ViewModelProviders.of(this).get(ListViewModel::class.java)
        listViewModel.refresh()

        myList.let {
            myListRecyclerView.layoutManager = LinearLayoutManager(context)
            myListRecyclerView.adapter = NewsListAdapter(it)
        }

        swipeRefreshLayout.setOnRefreshListener {
            swipeRefreshLayout.isRefreshing = true
            listViewModel.refresh()
            swipeRefreshLayout.isRefreshing = false
        }
        observeChanges()

    }



    private fun observeChanges() {
        listViewModel.loadError.observe(this, Observer { error ->
            error?.let {
                noDataTextView.visibility = if (it) View.VISIBLE else View.GONE
            }
        })

        listViewModel.showProgress.observe(this, Observer { showProgress ->
            showProgress?.let {
                progressBar.visibility = if (it) View.VISIBLE else View.GONE
                if (it) {
                    myListRecyclerView.visibility = View.GONE
                    noDataTextView.visibility = View.GONE
                }
            }
        })

        listViewModel.newData.observe(this, Observer { newsData ->
            newsData?.let {
                myListRecyclerView.visibility = View.VISIBLE
                adapter.updateList(newsData.articles as List<ArticlesItem>)
                println(newsData)
            }
        })
    }
}
