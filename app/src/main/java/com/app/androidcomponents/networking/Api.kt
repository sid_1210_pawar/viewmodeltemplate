package com.app.androidcomponents.networking

import com.app.androidcomponents.model.NewData
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {

    @GET("top-headlines")
    fun getNewsData(@Query("country") country: String, @Query("apiKey") apiKey: String): Single<NewData>
}