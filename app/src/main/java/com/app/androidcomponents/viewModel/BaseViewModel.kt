package com.app.androidcomponents.viewModel

import androidx.lifecycle.ViewModel

open class BaseViewModel : ViewModel() {


    override fun onCleared() {
        super.onCleared()
    }
}