package com.app.androidcomponents.viewModel

import androidx.lifecycle.MutableLiveData
import com.app.androidcomponents.model.NewData
import com.app.androidcomponents.networking.ApiService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

class ListViewModel : BaseViewModel() {


    val newData = MutableLiveData<NewData>()

    val loadError = MutableLiveData<Boolean>()

    val showProgress = MutableLiveData<Boolean>()

    private val apiService = ApiService()
    private val disposable = CompositeDisposable()


    //called in seperate method because we can simply call this method on swiperefresh layout keeping the main method private
    fun refresh() {
        fetchNews()
    }

    private fun fetchNews() {
        showProgress.value = true
        disposable.add(
            apiService.getList()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<NewData>() {
                    override fun onSuccess(t: NewData) {
                        showProgress.value = false
                        newData.value = t
                        loadError.value = false
                    }

                    override fun onError(e: Throwable) {
                        loadError.value = true
                        showProgress.value = false
                        e.printStackTrace()
                    }
                })
        )
    }
}